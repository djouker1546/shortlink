﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ShortLink.Services
{
    public interface IUserService
    {
        Task<IdentityUser> Get();
    }
}
