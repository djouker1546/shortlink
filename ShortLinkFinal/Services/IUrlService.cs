﻿using ShortLink.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLink.Services
{
    public interface IUrlService
    {
        Task ChangeStatus(int id, bool value);
        Task<Url> Add(Url url);
        Task AddClick(Url url);
        Task Delete(Url url);
        Task<Url> Get(int id);
        Task<IEnumerable<Url>> GetByUser(string userId);
    }
}
