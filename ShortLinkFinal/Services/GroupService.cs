﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ShortLink.Data;

namespace ShortLink.Services
{

    public class GroupService : IGruopService
    {
        private readonly AppDBContext appDBContext;

        public GroupService(AppDBContext appDBContext)
        {
            this.appDBContext = appDBContext;
        }

        public async Task<Group> Add(Group group)
        {
            await appDBContext.Groups.AddAsync(group);
            await appDBContext.SaveChangesAsync();
            return group;
        }

        public async Task Delete(Group groupa)
        {
            var group = await appDBContext.Groups.FirstAsync(x => x.Id == groupa.Id);
            appDBContext.Groups.Remove(group);
            await appDBContext.SaveChangesAsync();

        }

        public Task<Group> Get(int id)
        {
            var list = appDBContext.Groups.ToList();

            return appDBContext.Groups.FirstAsync(x => x.Id == id);
        }

        public async Task<Group> GetByGroupId(int groupId)
        {
            return await appDBContext.Groups.Where(g => g.Id == groupId).Include(g => g.Urls).FirstAsync();

        }

        public async Task<IEnumerable<Group>> GetByUserId(string userId)
        {
            return await appDBContext.Groups.Where(g => g.UserId == userId).Include(g => g.Urls).ToListAsync();

        }
    }
}
