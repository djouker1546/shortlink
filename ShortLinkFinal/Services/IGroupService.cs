﻿using ShortLink.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLink.Services
{
    public interface IGruopService
    {
        Task<Group> Add(Group group);
        Task Delete(Group groupa);
        Task<Group> Get(int id);
        Task<Group> GetByGroupId(int groupId);
        Task<IEnumerable<Group>> GetByUserId(string userId);
    }
}