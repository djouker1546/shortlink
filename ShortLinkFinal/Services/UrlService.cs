﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using ShortLink.Data;

namespace ShortLink.Services
{
    public class UrlService : IUrlService
    {
        private readonly AppDBContext appDBContext;
        private readonly HttpContext httpContext;
        

        public UrlService(AppDBContext appDBContext, IHttpContextAccessor httpContextAccessor)
        {
            this.appDBContext = appDBContext;
            this.httpContext = httpContextAccessor.HttpContext;
           
        }

        public async Task ChangeStatus(int id, bool value)
        {
            var url = await appDBContext.Urls.FirstAsync(u => u.Id == id);
            url.Active = value;
            await appDBContext.SaveChangesAsync();
        }

        public async Task<Url> Add(Url url)
        {

            await appDBContext.Urls.AddAsync(url);
            await appDBContext.SaveChangesAsync();
            //url.ShortUrl = $"{httpContext.Request.Host}/u/{url.Id.ToString()}";
            url.ShortUrl = $"u/{url.Id.ToString()}";

            await appDBContext.SaveChangesAsync();
            return url;
        }

        public async Task AddClick(Url url)
        {
            url.Clicks++;
            await appDBContext.SaveChangesAsync();
        }

        public async Task Delete(Url urla)
        {
            var url = await appDBContext.Urls.FirstAsync(x => x.Id == urla.Id);
            appDBContext.Urls.Remove(url);
            await appDBContext.SaveChangesAsync();
            
        }

        public Task<Url> Get(int id)
        {
            var list = appDBContext.Urls.ToList();

            return appDBContext.Urls.Include(u => u.Group).FirstAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Url>> GetByUser(string userId)
        {
            return await appDBContext.Groups.Where(g => g.UserId == userId).Include(g => g.Urls).SelectMany(g => g.Urls).ToListAsync();
            
        }
    }
}
