﻿var path = require('path');

module.exports = {
    entry: "./Client/app.js",
    mode: "development",
    output: {
        path: path.resolve(__dirname, './wwwroot/public'),
        publicPath: '/public/',
        filename: "bundle.js"
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules)/,
                loader: "babel-loader",
                options: {
                    presets: ["@babel/preset-env", "@babel/preset-react"]    // используемые плагины
                }
            }
        ]
    }
}