﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ShortLink.Data;
using ShortLink.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Security.Principal;
using Microsoft.AspNetCore.Authorization;

namespace ShortLink.Controllers
{
    [Authorize]
    [Route("api/group")]
    public class GroupController : Controller
    {


        private readonly IGruopService groupService;
        private readonly IUserService userService;

       
        public GroupController(IGruopService groupService, IUserService userService)
        {
            this.groupService = groupService;
            this.userService = userService;
        }       

        [HttpPost("add")]
        public async Task<IActionResult> Add([FromBody]Group group)
        {
            var user = await userService.Get();
            group.UserId = (await userService.Get()).Id;
            return Json(await groupService.Add(group));

        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var group = await groupService.Get(id);
            await groupService.Delete(group);
            return Ok();
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var group = await groupService.Get(id);
            return Json(group);

        }

        [HttpGet("getbygroupid/{groupid}")]
        public async Task<IActionResult> GetByGroupId(int groupId)
        {
            var group = await groupService.GetByGroupId(groupId);
            return Json(group);

        }

        [HttpGet("getbyuserid")]
        public async Task<IActionResult> GetByUserId()
        {
            var group = await groupService.GetByUserId((await userService.Get()).Id);
            return Json(group);

        }

        

    }
}
