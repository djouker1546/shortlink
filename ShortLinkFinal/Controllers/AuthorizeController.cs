﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ShortLink.Data;
using ShortLink.Services;
using ShortLink.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ShortLink.Controllers
{
    [Route("api/authorize")]
    public class AuthorizeController : Controller
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly UserManager<IdentityUser> userManager;

        public AuthorizeController(SignInManager<IdentityUser> signInManager, UserManager<IdentityUser> userManager)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
        }

        [HttpGet("check")]
        public IActionResult Check()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                return Ok();
            else
                return StatusCode(403);
        }

        [Authorize]
        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            return Ok();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(User user)
        {
            var result = await signInManager.PasswordSignInAsync($"{user.Login}@blank.blank", user.Password, true, lockoutOnFailure: true);
            return Json(result);

        }

        [HttpPost("signup")]
        public async Task<IActionResult> SignUp([FromBody]SignUpView signUpView)
        {
            var user = new IdentityUser { UserName = $"{signUpView.Login}@blank.blank", Email = $"{signUpView.Login}@blank.blank" };

            var result = await userManager.CreateAsync(user, signUpView.Password);
            if (result.Succeeded)
            {
                await signInManager.SignInAsync(user, isPersistent: false);
                return Ok();
            }
            return StatusCode(403);
        }

        public static string Encode(string text)
        {
            using (SHA512 shaM = new SHA512Managed())
            {
                return Encoding.UTF8.GetString(shaM.ComputeHash(Encoding.UTF8.GetBytes(text)));
            }
        }


    }
}

