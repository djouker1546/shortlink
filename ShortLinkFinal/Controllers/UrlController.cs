﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using ShortLink.Data;
using ShortLink.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLink.Controllers
{
    [Authorize]
    [Route("api/url")]
    public class UrlController : Controller
    {
        private readonly IUrlService urlService;
        private readonly IGruopService gruopService;
        private readonly IUserService userService;
        private readonly HttpContext httpContext;

        public UrlController(IUrlService urlService, IUserService userService, IGruopService gruopService, IHttpContextAccessor httpContextAccessor)
        {
            this.httpContext = httpContextAccessor.HttpContext;
            this.urlService = urlService;
            this.userService = userService;
            this.gruopService = gruopService;
        }

        [HttpPost("add")]
        public async Task<IActionResult> Add([FromBody]Url url)
        {
            var group = await gruopService.Get(url.GroupId);
            if (group.UserId != (await userService.Get()).Id)
                return BadRequest();
            return Json(await urlService.Add(url));

        }

        [HttpDelete("delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var url = await urlService.Get(id);
            if (url.Group.UserId != (await userService.Get()).Id)
                return BadRequest();
            await urlService.Delete(url);
            return Ok();
        }

        [HttpGet("get/{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var url = await urlService.Get(id);
            if (url.Group.UserId != (await userService.Get()).Id)
                return BadRequest();
            return Json(await urlService.Get(id));

        }

        [HttpPost("setstatus")]
        public async Task<IActionResult> SetStatus([FromBody]Url url)
        {
            await urlService.ChangeStatus(url.Id, url.Active);
            return Ok();
        }        

        [HttpGet("getbyuser")]
        public async Task<IActionResult> GetByUser()
        {
            var url = await urlService.GetByUser((await userService.Get()).Id);
            return Json(url);

        }

        [HttpGet("gethost")]
        public  string GetHost()
        {

            return httpContext.Request.Host.ToString();

        }

    }
}
