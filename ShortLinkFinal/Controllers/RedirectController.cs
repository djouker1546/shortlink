﻿using Microsoft.AspNetCore.Mvc;
using ShortLink.Services;
using System.Threading.Tasks;

namespace ShortLink.Controllers
{
    [Route("u")]
    public class RedirectController : Controller
    {
        private readonly IUrlService urlService;

        public RedirectController(IUrlService urlService)
        {
            this.urlService = urlService;
        }
        
        [HttpGet("{url}")]
        public async Task<IActionResult> Home(int url)
        {
            var urla = await urlService.Get(url);
            if (!urla.Active)
                return NotFound();
            await urlService.AddClick(urla);
            if (urla.LongUrl.StartsWith("//"))
                return Redirect(urla.LongUrl);
            return Redirect(!urla.LongUrl.StartsWith("http") ? "https://" + urla.LongUrl : urla.LongUrl);
        }
    }
}
