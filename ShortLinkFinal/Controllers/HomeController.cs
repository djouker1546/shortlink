﻿using Microsoft.AspNetCore.Mvc;

namespace ShortLink.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
