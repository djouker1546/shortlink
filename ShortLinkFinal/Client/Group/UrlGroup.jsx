﻿var React = require("react");
var ReactRouterDOM = require('react-router-dom');
const withRouter = ReactRouterDOM.withRouter;

import { connect } from 'react-redux';
import * as urlGroupActs from '../Store/Actions/GroupAction';
import { groupsAPI } from './../API.js'

const NavLink = ReactRouterDOM.NavLink;


class UrlGroup extends React.Component {
    constructor(props) {
        super(props);
        this.deleteId = 0;
        console.log(props);
    }  

    componentDidMount()
    {
        if (this.props.checkLogin() === false)
            this.props.history.push("/urlLogin");
        else
            this.getGroups();
    };

    getGroups()
    {                    
        groupsAPI.getGroupsAPI().then(response =>
            (this.props.getGroups(response.data)));
    };
    
    getGroupId(id)
    {      
        this.deleteId = id;  
    };
    
    deleteGroup()
    {     
        groupsAPI.deleteGroupAPI(this.deleteId).then(() => {
            this.getGroups();
            $("#deleteGroup").modal("hide");
        });                  
    };

    

    render() {
        const { groups } = this.props.groupsStore;   
        return (
            <div className="container">
                <div className="">
     
                    <div className="divh3"> <h3 className="h3Table" >Все Группы</h3></div>
                    {
                        <div>
                            <div className="container">
                                <div className="row py-2">
                                    {
                                        groups.map((group, index) => (
                                            <div className="col-4 py-2 px-2" key={'group' + group.id}>
                                                <div className="url-group py-2 px-2">
                                                    <h3>{group.title}</h3>
                                                    <NavLink to={"/urlSelectedGroup/" + group.id} className="btn btn-primary">Перейти</NavLink>
                                                    <button type="button" className="btn btn-outline-danger ml-2" data-toggle="modal" data-target="#deleteGroup" onClick={this.getGroupId.bind(this, group.id)}> Удалить </button>
                                                </div>
                                            </div>
                                        ))
                                    }
                                </div>
                            </div>
                        </div>
                    }


               
                        <div className="modal fade" id="deleteGroup" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <div className="modal-header">

                                        <h4 className="modal-title" id="myModalLabel">Удаление</h4>
                                    </div>
                                    <div className="modal-body">
                                        Вы действительно хотите удалить?
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Нет</button>
                                        <button type="button" className="btn btn-primary" onClick={this.deleteGroup.bind(this)}  >Да</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>
        );
    }



}

const mapStateToProps = state => ({
    groupsStore: state.groupsStore,
})

const mapDispatchToProps = dispatch => ({
    getGroups: groups => dispatch(urlGroupActs.getGroups(groups))
});

UrlGroup = withRouter(UrlGroup);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UrlGroup);