﻿var React = require("react");
var ReactRouterDOM = require('react-router-dom');

import { connect } from 'react-redux';
import * as urlGroupActs from '../Store/Actions/GroupAction';
import { groupsAPI,groupAPI } from './../API';
const withRouter = ReactRouterDOM.withRouter;

class UrlAddLink extends React.Component {
    constructor(props) {
        super(props);        
    }

    componentDidMount()
    {
        if (this.props.checkLogin() === false)
            this.props.history.push("/urlLogin");
        else
        this.getGroups();
    };

    getGroups() {
        groupsAPI.getGroupsAPI().then(response =>
                (this.props.getGroups(response.data)) 
        );   
    };


    redirectBack()
    {
        window.history.back();
    };
   
    addLink()
    {
        var newLink = {
            userId: 1,
            Title: this.linkTitle.value,
            LongUrl: this.longUrl.value,
            ShortUrl: "short",
            Active: true,
            GroupId: this.selectGroupId.value
        };

        if (this.selectGroupId.value == "")
        {
            $("#Wrong").html("Для добавления ссылки нужно добавить минимум одну группу!");
        }
        else
        {
            $("#Wrong").html(" ");
            console.log(this.selectGroupId.value);

            groupAPI.addLinkAPI(newLink).then(() => {
                this.props.history.push("/urlTable");
            });
        }
    };


    render() {        
        const { groups } = this.props.groupStore;
        return (

            <div className=" log">
                <h2 > Добавление Ссылки </h2>
                <div className="input-group mb-3 blockAddLink1">
                    <div className="input-group-prepend">

                        <span className="input-group-text">Вставьте ссылку</span>
                    </div>
                    <input type="text" className="form-control" placeholder="Your Link" ref={(input) => { this.longUrl = input; }}></input>
                </div>

                <div className="input-group blockAddLink2">
                    <div className="input-group-append">

                        <span className="input-group-text">Введите название</span>
                    </div>
                    <input type="text" className="form-control" placeholder="Link's title" ref={(input) => { this.linkTitle = input; }} ></input>
                </div>
                <h3 className="h3Style" > Выберите группу</h3>
                <select  ref={(input) => { this.selectGroupId = input; }} >
                    {
                        groups.map((group, index) => (
                            <option className="blockAddLink4" value={group.id} key={'group' + group.id} >
                                {group.title}
                            </option>
                        ))
                    }
                </select> 
                <div className="twoBtn">
                  <button className="btn btn-outline-success btnEnter mr-1" onClick={this.redirectBack.bind(this)} > Назад </button>
                    <button className="btn btn-outline-success btnEnter ml-1" onClick={this.addLink.bind(this)} > Сохранить </button>
                </div>
                <div id="Wrong"> </div>

                
            </div>          
        );
    }
}

const mapStateToProps = state => ({
    groupStore: state.groupsStore,
})
const mapDispatchToProps = dispatch => ({
    getGroups: groups => dispatch(urlGroupActs.getGroups(groups))
});

UrlAddLink = withRouter(UrlAddLink);
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UrlAddLink);