﻿import * as axios from "axios";


export const authorizeAPI =
{
    logoutAPI()
    {
        return axios.post("/api/authorize/logout");
    },

    loginAPI() {

    },

    signinAPI(newUser)
    {
        return axios({
            method: 'post',
            url: '/api/authorize/signup',
            data: newUser,
            headers: { 'Content-Type': 'application/json' }
        });     
    },


};

export const groupAPI =
{
    getHostAPI() {
        return axios.get('/api/url/gethost');
    },

    getGroupAPI(gid)
    {
        return axios.get('/api/group/getbygroupid/' + gid);
    },

    deleteUrlAPI(id)
    {
        return axios.delete('/api/url/delete/' + id);        

    },

    addLinkAPI(newLink)
    {
        return axios({
            method: 'post',
            url: '/api/url/add',
            data: newLink,
            headers: { 'Content-Type': 'application/json' }
        });
    },

    changeStatusAPI(changeData)
    {
        return axios({
            method: 'post',
            url: '/api/url/setstatus',
            data: changeData,
            headers: { 'Content-Type': 'application/json' }
        });
    },
   
};

export const groupsAPI =
{
    getGroupsAPI()
    {
        return axios.get("/api/group/getbyuserid");
    },

    deleteGroupAPI(id)
    {      
        return axios.delete("/api/group/delete/" + id );            
    },

    addGroupAPI(newGroup)
    { 
        return axios({
            method: 'post',
            url: '/api/group/add',
            data: newGroup,
            headers: { 'Content-Type': 'application/json' }
        });
    },

};





