﻿
import React from 'react';
import ReactDOM from "react-dom";
import configureStore from "./Store/store";
import { Provider } from "react-redux";
var ReactRouterDOM = require('react-router-dom');
import UrlTable from './Table/urlTable.jsx';
import UrlGroup from './Group/UrlGroup.jsx';
import UrlSelectedGroup from './SelectedGroup/urlSelectedGroup.jsx';
import UrlNavigation from './Navigation/urlNavigation.jsx';
import UrlAddLink from './addLink/urlAddLink.jsx';
import UrlLogin from './Login/urlLogin.jsx';
import UrlRegister from './Register/urlRegister.jsx';
import UrlAddGroup from './addGroup/urlAddGroup.jsx';
//var UrlAddGroup = require('./addGroup/urlAddGroup.jsx');
const Router = ReactRouterDOM.BrowserRouter;
const Route = ReactRouterDOM.Route;
const Switch = ReactRouterDOM.Switch;
const Redirect = ReactRouterDOM.Redirect;

var checkLogin = function () {
    var response = $.ajax({
        url: "/api/authorize/check",
        async: false
    });
    return response.status == 200;
};

ReactDOM.render(
    <Provider store={configureStore()}>
        <Router>
            <div>
                <UrlNavigation checkLogin={checkLogin} />
                <Switch>
                    <Route exact path="/">
                        <Redirect to="/urlLogin" />
                    </Route>

                    <Route exact path="/urlRegister" render={() => <UrlRegister />} />

                    <Route exact path="/urlLogin" render={() => <UrlLogin />} />

                    <Route exact path="/urlGroup" >
                        <UrlGroup checkLogin={checkLogin} />
                    </Route>

                    <Route exact path="/urlAddGroup"  >
                        <UrlAddGroup checkLogin={checkLogin} />
                    </Route>

                    <Route exact path="/urlAddLink" >
                        <UrlAddLink checkLogin={checkLogin} />
                    </Route>

                    <Route exact path="/urlSelectedGroup/:id" >
                        <UrlSelectedGroup checkLogin={checkLogin} />
                    </Route>

                    <Route exact path="/urlTable" >
                        <UrlTable checkLogin={checkLogin} />
                    </Route>
                </Switch>
            </div>
        </Router>
    </Provider>,

    document.getElementById("router")
);




