﻿var React = require("react");
var ReactRouterDOM = require('react-router-dom');
const withRouter = ReactRouterDOM.withRouter;
const NavLink = ReactRouterDOM.NavLink;

class UrlLogin extends React.Component {
    constructor(props) {
        super(props);
       
    }

    componentDidMount()
    {
        $("#AddGroup").css("display", "none");
        $("#AddLink").css("display", "none");
        $("#Logout").css("display", "none");
    };

    login()
    {
        var login = this.login.value;
        var password = this.password.value;
        if (login == "" || password == "") {

            $("#Wrong").html("Не введён Логин или Пароль!");
        }
        else {

            $("#Wrong").html("");
            /////////////////////////////////////////////////////////////////////////////
            var user = { login, password };
            var response = $.ajax({
                url: "/api/authorize/login",
                type: "post",
                async: false,
                data: user
            });
            if (response.status == 200) {
                $("#AddGroup").css("display", "inline-block");
                $("#AddLink").css("display", "inline-block");
                $("#Logout").css("display", "inline-block");
                this.props.history.push("/urlGroup");
            }
        }
    };

    notFound()
    {
        $("#Wrong").html("Пользователей с таким логином или паролем не обнаружено!");
    };

    render() {
        return (
            <div className=" log">
                <h2 > Авторизация </h2>
                <div className="input-group mb-3 blockLR1">
                    <div className="input-group-prepend">
                    
                        <span className="input-group-text">Логин</span>
                    </div>
                    <input type="text" className="form-control" placeholder="Username"  ref={(input) => { this.login = input; }}></input>
                 </div>

                <div className="input-group blockLR2">
                    <div className="input-group-append">
                        
                        <span className="input-group-text">Пароль</span>
                    </div>
                    <input type="password" className="form-control" placeholder="Password"   ref={(input) => { this.password = input; }}></input>
                </div>

                    <button className="btn btn-outline-success btnEnter" onClick={this.login.bind(this)} > Войти </button>

              
                <NavLink to="/urlRegister" className="btn btn-outline-success btnReg    " > Зарегестрироваться </NavLink>

                    <div id="Wrong">

                </div>
                </div>  
   
           
        );
    }
}

module.exports = withRouter(UrlLogin);