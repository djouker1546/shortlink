﻿var React = require("react");
var ReactRouterDOM = require('react-router-dom');
var ReactRouterDOM = require('react-router-dom');
const withRouter = ReactRouterDOM.withRouter;
const NavLink = ReactRouterDOM.NavLink;

import {authorizeAPI} from './../API';

class UrlNavigation extends React.Component {
    constructor(props) {
        super(props);
      
    }
    
    logout()
    {
        authorizeAPI.logoutAPI().then(() => {
            $("#logout").modal("hide");
            this.props.history.push("/urlLogin");
        });    
    }

     render() {
        return (
            <div>
                <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                   
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                               
                                <NavLink to="/urlGroup" className="nav-link"> Группы Ссылок  <span className="sr-only">(current)</span></NavLink>  
                                
                            </li>
                            <li className="nav-item active">
                               
                                <NavLink to="/urlTable" className="nav-link"> Все ссылки  <span className="sr-only">(current)</span></NavLink>
                            </li>
                            
                        </ul>

                        <ul className="navbar-nav ml-auto qqq" >
                            <NavLink id="AddGroup" to="/urlAddGroup" className="btn btn-info " > Добавить группу </NavLink>
                            <NavLink id="AddLink" to="/urlAddLink" className="btn btn-info btnAddLink" > Добавить Ссылку </NavLink>   
                            <button id="Logout" type="button" className="btn btn-info" data-toggle="modal" data-target="#logout" > Выйти </button>
                        </ul>
                    </div>
                </nav>    


                <div className="modal fade" id="logout" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">

                                <h4 className="modal-title" id="myModalLabel">Выход</h4>
                            </div>
                            <div className="modal-body">
                                Вы действительно хотите выйти?
                                </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Нет</button>
                                <button type="button" className="btn btn-primary" onClick={this.logout.bind(this)}  >Да</button>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        );
    }
}

UrlNavigation = withRouter(UrlNavigation);
export default (UrlNavigation)