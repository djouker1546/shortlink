﻿var React = require("react");
var ReactRouterDOM = require('react-router-dom');
const withRouter = ReactRouterDOM.withRouter;
import { authorizeAPI } from './../API';

class UrlRegister extends React.Component {
    constructor(props) {
        super(props);
       
    }

    componentDidMount()
    {
        $("#AddGroup").css("display", "none");
        $("#AddLink").css("display", "none");
        $("#Logout").css("display", "none");

    };

    registerShow()
    {
        $("#wrongRepeat").modal('hide');
        $(this.uPassword.value = "");
        $(this.u2Password.value = "");
        this.props.history.push("/urlRegister");


    };

    addUser()
    {
       
        if (this.uLogin.value == "" || this.uPassword.value == "" || this.u2Password.value == "") {

            $("#Wrong").html("Не введён Логин или Пароль!");
        }
        else
        {
            $("#Wrong").html("");
            var newUser = {
                Login: this.uLogin.value,
                Password: this.uPassword.value,
                ConfirmPassword: this.u2Password.value
            };
            
            if (newUser.Password === newUser.ConfirmPassword)
            {
                authorizeAPI.signinAPI(newUser).then(() => {
                    this.props.history.push("/urlLogin");
                });
            
            }
            else {
                $("#wrongRepeat").modal('show');

            }
        }
    };

    redirectBack()
    {
        window.history.back();
    };

    render() {
        return (

            <div className=" reg">
                <h2 > Регистрация </h2>
                <div className="input-group mb-3 blockLR1">
                    <div className="input-group-prepend">

                        <span className="input-group-text">Логин</span>
                    </div>
                    <input type="text" className="form-control" placeholder="Username" ref={(input) => { this.uLogin = input; }}></input>
                </div>

                <div className="input-group blockLR2">
                    <div className="input-group-append">

                        <span className="input-group-text">Пароль</span>
                    </div>
                    <input type="password" className="form-control" placeholder="Password" ref={(input) => { this.uPassword = input; }}></input>
                </div>

                <div className="input-group blockR3">
                    <div className="input-group-append">

                        <span className="input-group-text">Повторите пароль</span>
                    </div>
                    <input type="password" className="form-control" placeholder="Repeat password" ref={(input) => { this.u2Password = input; }}></input>
                </div>
                <div className="twoBtn">
                    <button type="button" className="btn btn-outline-success btnEnter mr-1" onClick={this.redirectBack.bind(this)}  > Войти </button>
                        <button className="btn btn-outline-success btnEnter" onClick={this.addUser.bind(this)}> Сохранить </button>
                </div>

                <div id="Wrong">


                </div>



                <div className="modal fade" id="wrongRepeat" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">

                                <h4 className="modal-title" id="wrongRepeat">Ошибка!</h4>
                            </div>
                            <div className="modal-body">
                                Повторный пароль не соответствует введенному!
                            </div>
                            <div className="modal-footer">

                               
                                <button type="button" className="btn btn-primary" onClick={this.registerShow.bind(this)}  > Ок </button>

                            </div>
                        </div>
                    </div>
                </div>


            </div>
        );
    }
}

UrlRegister = withRouter(UrlRegister);
export default UrlRegister;