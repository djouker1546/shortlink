﻿import { combineReducers } from "redux";
import GroupReducer from "./Reducers/GroupReducer";

export default combineReducers({
    groupsStore: GroupReducer
});