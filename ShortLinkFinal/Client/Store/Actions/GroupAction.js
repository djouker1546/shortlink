﻿var getGroups = function (groups) {
    return {
        type: "GET_GROUPS",
        groups: groups
    }
};


var getGroup = function (group) {
    return {
        type: "GET_GROUP",
        group: group
    }
};
module.exports = { getGroups, getGroup };