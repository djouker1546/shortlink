﻿import { applyMiddleware, createStore } from 'redux';

import RootReducer from './RootReducer';


export default function configureStore() {
    return createStore(RootReducer, applyMiddleware());
}
