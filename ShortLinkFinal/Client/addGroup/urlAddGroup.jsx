﻿import { groupsAPI } from "../API";

   var React = require("react");
var ReactRouterDOM = require('react-router-dom');
const withRouter = ReactRouterDOM.withRouter;


class UrlAddGroup extends React.Component {
    constructor(props) {
        super(props);     
       
    }

    componentDidMount()
    {
        if (this.props.checkLogin() === false)
            this.props.history.push("/urlLogin");
    };

    redirectBack()
    {
        window.history.back();
    };
    
    addGroup() {
        
        var newGroup = {
                userId: 1,
                title: this.groupTitle.value
        };

        groupsAPI.addGroupAPI(newGroup).then( ()=> {
            
            this.props.history.push("/urlGroup");
        });          
    };
    
    render() {
       
        return (
            <div className=" reg">
                <h2 > Добавление Группы </h2>
                <div className="input-group mb-3 blockAddGroup">
                    <div className="input-group-prepend">

                        <span className="input-group-text">Название группы</span>
                    </div>
                    <input type="text" className="form-control" placeholder="Group's title" ref={(input) => { this.groupTitle = input; }}></input>
                </div>
                <div className="twoBtn">
                    <button className="btn btn-outline-success btnEnter mr-1" onClick={this.redirectBack.bind(this)} > Назад </button>
                    <button className="btn btn-outline-success btnEnter" onClick={this.addGroup.bind(this)} > Сохранить </button>
                </div>
            </div>
        );
    }
}
    UrlAddGroup = withRouter(UrlAddGroup);
export default UrlAddGroup;
