﻿var React = require("react");
var queryString = require("query-string");
var ReactRouterDOM = require('react-router-dom');

import { connect } from 'react-redux';
import * as urlGroupActs from '../Store/Actions/GroupAction';
import { groupAPI } from '../API';

const withRouter = ReactRouterDOM.withRouter;


class UrlSelectedGroup extends React.Component {
    constructor(props) {
        super(props);
        this.state = { group: { id: null, title: null, urls: [{ clicks : null, active: null, title: null, shortUrl: null, longUrl: null }] } };
        this.deleteId = 0;
        this.changeStatus = this.changeStatus.bind(this);
        
            
        
        
    }

    componentDidMount()
    {
        if (this.props.checkLogin() === false)
            this.props.history.push("/urlLogin");
        else
        {
            this.getGroup();
            this.getHost();
        }
    };

    getHost()
    {
        groupAPI.getHostAPI().then((response) => {
            console.log(response);
            this.hostString = response.data;
        });
        
    };
   // + httpContext.Request.Host 
    getGroup()
    {
        var u = window.location.href.toString();
        var gid = u.slice(u.lastIndexOf('/') + 1, u.length);

        groupAPI.getGroupAPI(gid).then((response) => {
            this.setState({ group: response.data });
           

        });

    };
  
    getUrlId(id)
    {         
        this.deleteId = id;
    };

    deleteUrl()
    {
        groupAPI.deleteUrlAPI(this.deleteId).then(() => {
            this.getGroup();
            $("#deleteUrl").modal("hide");
        });
       
    };

    changeStatus(id, value)
    {
        var changeData = {
            id: id, active: value
        };

        groupAPI.changeStatusAPI(changeData).then(() => {
            this.getGroup();
        });
    };    

    render()
    {             
        const group = this.state.group;               
        return (
            <div className="container" >

                
                <div >
                    
                    <div className="divh3">
                        <h3 className="h3Table" >Группа  {group.title} </h3>
                    </div>
                    
            
                      <div>
                             
                        <table className="table table-hover thead-light container">
                                <thead>
                                    <tr>
                                        <td> Статус </td>
                                        <td> Название </td>
                                        <td> Переходы </td>
                                        <td> Длинная Ссылка </td>
                                <td> Короткая Ссылка </td >
                                <td>Удаление</td>
                                    </tr >
                                </thead>
                                <tbody>
                                    {
                                    group.urls.map((item, index1) => (
                                        <tr className="row1" key={'item' + item.id}>
                                                    <td>
                                                        <div className="custom-control custom-switch">
                                                            <input type="checkbox" className="custom-control-input" checked={item.active}
                                                                onChange={this.changeStatus.bind(this, item.id, !item.active)} id={"switch-" + item.id}
                                                            />
                                                            <label className="custom-control-label" htmlFor={"switch-" + item.id}></label>
                                                        </div>
                                                    </td>
                                                    <td>{item.title} </td >
                                                    <td>{item.clicks}</td>
                                            <td className="col1"> <a className="links" target="_blank" href={item.longUrl}> {item.longUrl} </a></td >
                                            <td><a className="links" target="_blank" href={"https://" + this.hostString + "/" + item.shortUrl}> {item.shortUrl} </a></td >
                                                    <td> <button type="button" className="btn btn-outline-danger" data-toggle="modal" data-target="#deleteUrl"
                                                                onClick={this.getUrlId.bind(this, item.id)} > Удалить
                                                        </button>
                                                    </td>
                                       </tr>
                                                    ))
                                                }
                                </tbody>
                        </table>
                    </div>


                    <div className="modal fade" id="deleteUrl" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">

                                    <h4 className="modal-title" id="myModalLabel">Удаление</h4>
                                </div>
                                <div className="modal-body">
                                    Вы действительно хотите удалить?
                                </div>
                                <div className="modal-footer">
                                    <button type="button" className="btn btn-secondary" data-dismiss="modal">Нет</button>
                                    <button type="button" className="btn btn-primary" onClick={this.deleteUrl.bind(this)}  >Да</button>
                                </div>
                            </div>
                        </div>
                    </div>



                </div> 
            </div>
                    
        );

    }
}
UrlSelectedGroup = withRouter(UrlSelectedGroup);
const mapStateToProps = state => ({
    groupStore: state.groupsStore,
})

const mapDispatchToProps = dispatch => ({
    getGroup: groups => dispatch(urlGroupActs.getGroups(groups))
});    
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UrlSelectedGroup);