﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLink.Data
{
    public class Group
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string UserId { get; set; }

        public virtual User User { get; set; }
        public virtual List<Url> Urls { get; set; }
    }
}
