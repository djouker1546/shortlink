﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ShortLink.Data
{
    public class Url
    {
        public int Id { get; set; }  
        public string Title { get; set; }
        public string LongUrl { get; set; }
        public string ShortUrl { get; set; }
        public bool Active { get; set; }
        public int Clicks { get; set; }
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }

    }
}
