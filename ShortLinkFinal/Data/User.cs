﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ShortLink.Data
{
    public class User
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        
        public virtual List<Url> Urls { get; set; }

        public User() : base()
        {

        }
    }

    public class UserRole : IdentityRole<int>
    {

    }

    public class UserRolesService : IRoleStore<IdentityRole>
    {
        private readonly AppDBContext dbContext;

        public UserRolesService(AppDBContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<IdentityResult> CreateAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            dbContext.Add(role);
            await dbContext.SaveChangesAsync();
            return new IdentityResult();
        }

        public async Task<IdentityResult> DeleteAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            dbContext.Remove(role);
            await dbContext.SaveChangesAsync();
            return new IdentityResult();
        }

        public void Dispose()
        {

        }

        public async Task<IdentityRole> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            var role = await dbContext.UserRoles.FirstAsync(x => x.Id.ToString() == roleId);
            return new IdentityRole { Id = roleId, ConcurrencyStamp = Guid.NewGuid().ToString(), Name = role.Name, NormalizedName = role.NormalizedName };
        }

        public async Task<IdentityRole> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            var role = await dbContext.UserRoles.FirstAsync(x => x.Name == normalizedRoleName);
            return new IdentityRole { Id = role.Id.ToString(), ConcurrencyStamp = Guid.NewGuid().ToString(), Name = role.Name, NormalizedName = role.NormalizedName };
        }

        public async Task<string> GetNormalizedRoleNameAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            return role.NormalizedName;
        }

        public async Task<string> GetRoleIdAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            return role.Id.ToString();
        }

        public async Task<string> GetRoleNameAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            return role.Name.ToString();
        }

        public async Task SetNormalizedRoleNameAsync(IdentityRole role, string normalizedName, CancellationToken cancellationToken)
        {
            role.NormalizedName = normalizedName;
        }

        public async Task SetRoleNameAsync(IdentityRole role, string roleName, CancellationToken cancellationToken)
        {
            role.Name = roleName;
        }

        public async Task<IdentityResult> UpdateAsync(IdentityRole role, CancellationToken cancellationToken)
        {
            dbContext.Update(role);
            await dbContext.SaveChangesAsync();
            return new IdentityResult();
        }
    }
}
