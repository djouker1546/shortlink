﻿using ShortLink.Controllers;
using ShortLink.Services;
using Xunit;
using Microsoft.AspNetCore.Identity;
using System.Web.Mvc;
using ShortLink.Data;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ContentResult = Microsoft.AspNetCore.Mvc.ContentResult;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ShortLink.Tests

{
   public class UrlControllerTests
    {

       [Fact]

        public void TestGetUrl()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "GolfClub",
                UserId = 1.ToString()
            };

            var testurl = new Url
            {
                Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
                Group = testgroup
            };

            var user1 = new IdentityUser { Id = 1.ToString() };

            // Arrange
            var mockU = new Mock<IUserService>();
            var mockG = new Mock<IGruopService>();
            var mock = new Mock<IUrlService>();

            mockU.Setup(repo => repo.Get()).Returns(Task.FromResult(user1));      
            mockG.Setup(repo => repo.Get(testgroup.Id)).Returns(Task.FromResult(testgroup));           
            mock.Setup(repo => repo.Get(testurl.Id)).Returns(Task.FromResult(testurl));

            UrlController controller = new UrlController(mock.Object, mockU.Object, mockG.Object);

            // Act  
            var result = controller.Get(testurl.Id);

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);
            var model = Assert.IsAssignableFrom<Url>(((Microsoft.AspNetCore.Mvc.JsonResult)viewResult.Result).Value);

        }

        [Fact]

        public void TestGetByUser()
        {
            
            var testgroup = new Group
            {
                Id = 100,
                Title = "GolfClub",
                UserId = 1.ToString()
            };

            var testurl = new Url
            {
                Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
                Group = testgroup
            };

            var user1 = new IdentityUser { Id = 1.ToString() };

           
            // Arrange
            var mockU = new Mock<IUserService>();
            var mockG = new Mock<IGruopService>();
            var mock = new Mock<IUrlService>();

            mockU.Setup(repo => repo.Get()).Returns(Task.FromResult(user1));            
            mockG.Setup(repo => repo.Get(testgroup.Id)).Returns(Task.FromResult(testgroup));
            mock.Setup(repo => repo.GetByUser("1")).Returns(GetTestUrls());

            UrlController controller = new UrlController(mock.Object, mockU.Object, mockG.Object);

            // Act  
            var result = controller.GetByUser();

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Url>>(((Microsoft.AspNetCore.Mvc.JsonResult)viewResult.Result).Value);
            Assert.Equal(GetTestUrls().Result.Count(), model.Count());

        }
        private Task<IEnumerable<Url>> GetTestUrls()
        {
            IEnumerable<Url> urls = new List<Url>
            {
                new Url {  Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
                },
                new Url {  Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
               },
                new Url { Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
                 }
                
            };
            return Task.FromResult(urls); 
        }

        [Fact]

        public void TestAddUrl()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            var testurl = new Url
            {
                Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
                Group = testgroup
            };

            var user1 = new IdentityUser { Id = 1.ToString() };

            // Arrange
            var mock = new Mock<IUrlService>();
            var mockU = new Mock<IUserService>();
            var mockG = new Mock<IGruopService>();

            mock.Setup(repo => repo.Add(testurl)).Returns(AddTestUrl());
            mockU.Setup(repo => repo.Get()).Returns(Task.FromResult(user1));
            mockG.Setup(repo => repo.Get(testgroup.Id)).Returns(Task.FromResult(testgroup));
                     
            UrlController controller = new UrlController(mock.Object, mockU.Object,mockG.Object);

            // Act  
            var result = controller.Add(testurl);

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);           
            var model = Assert.IsAssignableFrom<Url>(((Microsoft.AspNetCore.Mvc.JsonResult)viewResult.Result).Value);

        }
        private Task<Url> AddTestUrl()
        {
            var testurl = new Url
            {
                Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
               
            };
            return Task.FromResult(testurl);
        }


        [Fact]

        public void TestDeleteUrl()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            var testurl = new Url
            {
                Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
                Group = testgroup
            };

            var user1 = new IdentityUser { Id = 1.ToString() };

            // Arrange
            var mock = new Mock<IUrlService>();
            var mockU = new Mock<IUserService>();
            var mockG = new Mock<IGruopService>();

            mockG.Setup(repo => repo.Get(testgroup.Id)).Returns(Task.FromResult(testgroup));
            mock.Setup(repo => repo.Delete(testurl)).Returns(DeleteTestUrl());
            mockU.Setup(repo => repo.Get()).Returns(Task.FromResult(user1));

            GroupController controller = new GroupController(mockG.Object, mockU.Object);

            // Act  
            var result = controller.Delete(testgroup.Id);

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);           

        }
        private Task<Url> DeleteTestUrl()
        {
            var testurl = new Url
            {
                Id = 200,
                Title = "Golf",
                LongUrl = "www.Golfclub.com",
                ShortUrl = "u200",
                Active = true,
                Clicks = 0,
                GroupId = 100,
                
            };

            return Task.FromResult(testurl);
        }


    };
}