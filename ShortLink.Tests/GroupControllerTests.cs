﻿using ShortLink.Controllers;
using ShortLink.Services;
using Xunit;
using Microsoft.AspNetCore.Identity;
using System.Web.Mvc;
using ShortLink.Data;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ContentResult = Microsoft.AspNetCore.Mvc.ContentResult;
using System.Security.Claims;
using System.Collections.Generic;
using System.Linq;

namespace ShortLink.Tests
{
    public class GroupControllerTests
    {     

        [Fact]

        public void TestGetGroup()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            // Arrange
            var mock = new Mock<IGruopService>();
            mock.Setup(repo => repo.Get(testgroup.Id)).Returns(GetTestGroup());

            GroupController controller = new GroupController(mock.Object, null);

            // Act  
            var result = controller.Get(testgroup.Id);

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);  
            var model = Assert.IsAssignableFrom<Group>(((Microsoft.AspNetCore.Mvc.JsonResult)viewResult.Result).Value);

        }
        private Task<Group> GetTestGroup()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            return Task.FromResult(testgroup);
        }

        [Fact]

        public void TestAddGroup()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            var user1 = new IdentityUser { Id = 1.ToString() };

            // Arrange
            var mock = new Mock<IGruopService>();
            var mockU = new Mock<IUserService>();

            mock.Setup(repo => repo.Add(testgroup)).Returns(AddTestGroup());
            mockU.Setup(repo => repo.Get()).Returns(Task.FromResult(user1));

            GroupController controller = new GroupController(mock.Object, mockU.Object);

            // Act  
            var result = controller.Add(testgroup);

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);
            var model = Assert.IsAssignableFrom<Group>(((Microsoft.AspNetCore.Mvc.JsonResult)viewResult.Result).Value);

        }
        private Task<Group> AddTestGroup()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            return Task.FromResult(testgroup);
        }

        [Fact]

        public void TestDeleteGroup()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            var user1 = new IdentityUser { Id = 1.ToString() };

            // Arrange
            var mock = new Mock<IGruopService>();
            var mockU = new Mock<IUserService>();

            mock.Setup(repo => repo.Delete(testgroup)).Returns(DeleteTestGroup());
            mockU.Setup(repo => repo.Get()).Returns(Task.FromResult(user1));

            GroupController controller = new GroupController(mock.Object, mockU.Object);

            // Act  
            var result = controller.Delete(testgroup.Id);

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);            

        }
        private Task<Group> DeleteTestGroup()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            return Task.FromResult(testgroup);
        }


        [Fact]

        public void TestGetByGroupId()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            // Arrange
            var mock = new Mock<IGruopService>();
            mock.Setup(repo => repo.GetByGroupId(testgroup.Id)).Returns(GetTestGroup());

            GroupController controller = new GroupController(mock.Object, null);

            // Act  
            var result = controller.GetByGroupId(testgroup.Id);

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);
            var model = Assert.IsAssignableFrom<Group>(((Microsoft.AspNetCore.Mvc.JsonResult)viewResult.Result).Value);

        }

        [Fact]

        public void TestGetByUserId()
        {
            var testgroup = new Group
            {
                Id = 100,
                Title = "Golf",
                UserId = 1.ToString()
            };

            var user1 = new IdentityUser { Id = 1.ToString() };

            // Arrange
            var mockU = new Mock<IUserService>();
            var mock = new Mock<IGruopService>();

            mockU.Setup(repo => repo.Get()).Returns(Task.FromResult(user1));
            mock.Setup(repo => repo.GetByUserId(testgroup.UserId)).Returns(GetTestGroups());

            GroupController controller = new GroupController(mock.Object, mockU.Object);

            // Act  
            var result = controller.GetByUserId();

            // Assert
            var viewResult = Assert.IsType<Task<IActionResult>>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Group>>(((Microsoft.AspNetCore.Mvc.JsonResult)viewResult.Result).Value);
            Assert.Equal(GetTestGroups().Result.Count(), model.Count());           

        }
        private Task<IEnumerable<Group>> GetTestGroups()
        {
            IEnumerable<Group> groups = new List<Group>
            {
                new Group {  Id = 100,
                    Title = "Golf",
                    UserId = 1.ToString()},
                new Group {  Id = 101,
                    Title = "BMW",
                    UserId = 1.ToString()},
                new Group {  Id = 102,
                    Title = "Merc",
                    UserId = 1.ToString()},
                new Group {  Id = 103,
                    Title = "Audi",
                    UserId = 1.ToString()}
            };
            return Task.FromResult(groups); ;
        }
    }  
}
